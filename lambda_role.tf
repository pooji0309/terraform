

resource "aws_iam_role" "iam_for_lambda1" {
  name               = "iam_for_lambda1"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_policy.json}"
}
