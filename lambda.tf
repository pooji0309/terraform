




resource "aws_lambda_function" "hello_lambda" {
 
  function_name = "hello_lambda"
  filename         = "${data.archive_file.zip.output_path}"
  source_code_hash = "${data.archive_file.zip.output_base64sha256}"

  role    = "${aws_iam_role.iam_for_lambda1.arn}"
  handler = "hello_lambda.hello"
  runtime = "python3.6"

  
}
