# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
  access_key = ""
  secret_key = ""
}




provider "archive" {}

data "archive_file" "zip" {
  type        = "zip"

  source_file = "hello_lambda.py"
  output_path = "hello_lambda.zip"
}
